#ifndef TRASH_MOUNT_INFO_HPP
#define TRASH_MOUNT_INFO_HPP

#include <cassert>
#include <concepts>
#include <source_location>
#include <string>
#include <string_view>
#include <system_error>

namespace stream9::linux::proc::mountinfo {

// /proc/[pid]/mountinfo
//
// 36 35 98:0 /mnt1 /mnt2 rw,noatime master:1 - ext3 /dev/root rw,errors=continue
// (1)(2)(3)   (4)   (5)      (6)      (7)   (8) (9)   (10)         (11)

// GRAMMAR
// mountinfo = *(entry cr)
// entry = mount_id parent_id device_id root mount_point
//         mount_options optional_fields filesystem_type
//         mount_source super_options
//
// (1) mount_id = 1*char
// (2) parent_id = 1*char
// (3) device_id = device_major ":" device_minor
// (4) root = 1*char
// (5) mount_point = 1*char
// (6) mount_options = mount_option *("," mount_option)
// (7) optional_fields = *(optional_field) "-"
// (9) filesystem_type = 1*(char - ".")  *1("." filesystem_subtype)
// (10) mount_source = 1*char
// (11) super_options = mount_option *("," mount_option)
//
// device_major = 1*(char - ":")
// device_minor = 1*char
//
// mount_option = 1*(char - "," - "=") *1("=" mount_option_value)
// mount_option_value = 1*(char - ",")
//
// optional_field = optional_tag *1(":" optional_value)
// optional_tag = 1*(char - ":")
// optional_value = 1*char
//
// filesystem_subtype = 1*char
//
// char = any ASCII character - (space | cr)
// space = ' ' / \t
// cr = \n

template<typename EventHandler>
bool
parse(std::string_view text, EventHandler&&);

/*
// all handler functions are optional
struct event_handler_example
{
    void begin_entry();

    // (1) mount ID
    void on_mount_id(std::string_view);

    // (2) parent ID
    void on_parent_id(std::string_view);

    // (3) device ID major:minor
    // ex. "98:0" when on_device_minor is not defined
    //     "98" when on_device_minor is defined
    void on_device_id(std::string_view);

    // ex. "0"
    void on_device_minor(std::string_view);

    // (4) root
    void on_root(std::string_view);

    // (5) mount point
    void on_mount_point(std::string_view);

    // (6) mount options
    // ex. "rw,noatime,errors=continue"
    void on_mount_options(std::string_view);

    // ex. "errors=continue" when on_mount_option_value is not defined
    //     "errors" when on_mount_option_value is defined
    void on_mount_option(std::string_view);

    // ex. "continue"
    void on_mount_option_value(std::string_view);

    // (7) optional fields
    // ex. "master:1 shared:2"
    void on_optional_fields(std::string_view);

    // ex. "master:1" if on_optional_value is not defined
    //     "master" if on_optional_value is defined
    void on_optional_field(std::string_view);

    // ex. "1"
    void on_optional_value(std::string_view);

    // (9) filesystem type
    // ex. "fuse.encfs" when on_filesystem_subtype is not defined
    //     "fuse" when on_filesystem_subtype is defined
    void on_filesystem_type(std::string_view);

    // ex. "encfs"
    void on_filesystem_subtype(std::string_view);

    // (10) mount source
    void on_mount_source(std::string_view);

    // (11) super options
    // ex. "rw,errors=continue"
    // each option will be reported via on_mount_option and on_mount_option_value
    void on_super_options(std::string_view);

    // ex. "errors=continue" when on_super_option_value is not defined
    //     "errors" when on_super_option_value is defined
    void on_super_option(std::string_view);

    // ex. "continue"
    void on_super_option_value(std::string_view);

    void on_end_entry();

    void on_error(std::error_code,
                  std::string_view::const_iterator,
                  std::source_location);

    void on_new_line(std::string_view::const_iterator);
};
*/

enum class errc {
    premature_end_of_text,
    premature_end_of_line,
    invalid_character,
};

// classes to throw from event handler
struct exit {};
struct skip_line {};

} // namespace stream9::linux::proc::mountinfo

#endif // TRASH_MOUNT_INFO_HPP

#include "mountinfo.ipp"
