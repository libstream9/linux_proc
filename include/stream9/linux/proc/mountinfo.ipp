#ifndef STREAM9_LINUX_PROC_MOUNTINFO_IPP
#define STREAM9_LINUX_PROC_MOUNTINFO_IPP

namespace stream9::linux::proc::mountinfo {

//TODO error handling

namespace parser { //TODO noexcept

    using iterator = std::string_view::iterator;

    template<typename T>
    concept has_begin_entry = requires (T h, iterator i) {
        h.begin_entry(i);
    };

    // (1) mount ID
    template<typename T>
    concept has_on_mount_id = requires (T h, std::string_view s) {
        h.on_mount_id(s);
    };

    // (2) parent ID
    template<typename T>
    concept has_on_parent_id = requires (T h, std::string_view s) {
        h.on_parent_id(s);
    };

    // (3) device ID major:minor
    template<typename T>
    concept has_on_device_id = requires (T h, std::string_view s) {
        h.on_device_id(s);
    };

    template<typename T>
    concept has_on_device_major = requires (T h, std::string_view s) {
        h.on_device_major(s);
    };

    template<typename T>
    concept has_on_device_minor = requires (T h, std::string_view s) {
        h.on_device_minor(s);
    };

    // (4) root
    template<typename T>
    concept has_on_root = requires (T h, std::string_view s) {
        h.on_root(s);
    };

    // (5) mount point
    template<typename T>
    concept has_on_mount_point = requires (T h, std::string_view s) {
        h.on_mount_point(s);
    };

    // (6) mount options
    template<typename T>
    concept has_on_mount_options = requires (T h, std::string_view s) {
        h.on_mount_options(s);
    };

    template<typename T>
    concept has_on_mount_option = requires (T h, std::string_view s) {
        h.on_mount_option(s);
    };

    template<typename T>
    concept has_on_mount_option_value = requires (T h, std::string_view s) {
        h.on_mount_option_value(s);
    };

    // (7) optional fields
    template<typename T>
    concept has_on_optional_fields = requires (T h, std::string_view s) {
        h.on_optional_fields(s);
    };

    template<typename T>
    concept has_on_optional_field = requires (T h, std::string_view s) {
        h.on_optional_field(s);
    };

    template<typename T>
    concept has_on_optional_value = requires (T h, std::string_view s) {
        h.on_optional_value(s);
    };

    // (9) filesystem type
    template<typename T>
    concept has_on_filesystem_type = requires (T h, std::string_view s) {
        h.on_filesystem_type(s);
    };

    template<typename T>
    concept has_on_filesystem_subtype = requires (T h, std::string_view s) {
        h.on_filesystem_subtype(s);
    };

    // (10) mount source
    template<typename T>
    concept has_on_mount_source = requires (T h, std::string_view s) {
        h.on_mount_source(s);
    };

    // (11) super options
    template<typename T>
    concept has_on_super_options = requires (T h, std::string_view s) {
        h.on_super_options(s);
    };

    template<typename T>
    concept has_on_super_option = requires (T h, std::string_view s) {
        h.on_super_option(s);
    };

    template<typename T>
    concept has_on_super_option_value = requires (T h, std::string_view s) {
        h.on_super_option_value(s);
    };

    template<typename T>
    concept has_end_entry = requires (T h, iterator i) {
        h.end_entry(i);
    };

    template<typename T>
    concept has_on_error =
        requires (T h,
                  std::error_code e,
                  iterator i,
                  std::source_location l)
    {
        h.on_error(e, i, l);
    };

    template<typename T>
    concept has_on_new_line = requires (T h, iterator it) {
        h.on_new_line(it);
    };

    template<typename T>
    void
    emit_begin_entry(T& h, iterator const it)
    {
        if constexpr (has_begin_entry<T>) {
            h.begin_entry(it);
        }
    }

    template<typename T>
    void
    emit_mount_id(T& h, std::string_view const s)
    {
        if constexpr (has_on_mount_id<T>) {
            h.on_mount_id(s);
        }
    }

    template<typename T>
    void
    emit_parent_id(T& h, std::string_view const s)
    {
        if constexpr (has_on_parent_id<T>) {
            h.on_parent_id(s);
        }
    }

    template<typename T>
    void
    emit_device_id(T& h, std::string_view const s)
    {
        if constexpr (has_on_device_id<T>) {
            h.on_device_id(s);
        }
    }

    template<typename T>
    void
    emit_device_major(T& h, std::string_view const s)
    {
        if constexpr (has_on_device_major<T>) {
            h.on_device_major(s);
        }
    }

    template<typename T>
    void
    emit_device_minor(T& h, std::string_view const s)
    {
        if constexpr (has_on_device_minor<T>) {
            h.on_device_minor(s);
        }
    }

    template<typename T>
    void
    emit_root(T& h, std::string_view const s)
    {
        if constexpr (has_on_root<T>) {
            h.on_root(s);
        }
    }

    template<typename T>
    void
    emit_mount_point(T& h, std::string_view const s)
    {
        if constexpr (has_on_mount_point<T>) {
            h.on_mount_point(s);
        }
    }

    template<typename T>
    void
    emit_mount_options(T& h, std::string_view const s)
    {
        if constexpr (has_on_mount_options<T>) {
            h.on_mount_options(s);
        }
    }

    template<typename T>
    void
    emit_mount_option(T& h, std::string_view const s)
    {
        if constexpr (has_on_mount_option<T>) {
            h.on_mount_option(s);
        }
    }

    template<typename T>
    void
    emit_mount_option_value(T& h, std::string_view const s)
    {
        if constexpr (has_on_mount_option_value<T>) {
            h.on_mount_option_value(s);
        }
    }

    template<typename T>
    void
    emit_optional_fields(T& h, std::string_view const s)
    {
        if constexpr (has_on_optional_fields<T>) {
            h.on_optional_fields(s);
        }
    }

    template<typename T>
    void
    emit_optional_field(T& h, std::string_view const s)
    {
        if constexpr (has_on_optional_field<T>) {
            h.on_optional_field(s);
        }
    }

    template<typename T>
    void
    emit_optional_value(T& h, std::string_view const s)
    {
        if constexpr (has_on_optional_value<T>) {
            h.on_optional_value(s);
        }
    }

    template<typename T>
    void
    emit_filesystem_type(T& h, std::string_view const s)
    {
        if constexpr (has_on_filesystem_type<T>) {
            h.on_filesystem_type(s);
        }
    }

    template<typename T>
    void
    emit_filesystem_subtype(T& h, std::string_view const s)
    {
        if constexpr (has_on_filesystem_subtype<T>) {
            h.on_filesystem_subtype(s);
        }
    }

    template<typename T>
    void
    emit_mount_source(T& h, std::string_view const s)
    {
        if constexpr (has_on_mount_source<T>) {
            h.on_mount_source(s);
        }
    }

    template<typename T>
    void
    emit_super_options(T& h, std::string_view const s)
    {
        if constexpr (has_on_super_options<T>) {
            h.on_super_options(s);
        }
    }

    template<typename T>
    void
    emit_super_option(T& h, std::string_view const s)
    {
        if constexpr (has_on_super_option<T>) {
            h.on_super_option(s);
        }
    }

    template<typename T>
    void
    emit_super_option_value(T& h, std::string_view const s)
    {
        if constexpr (has_on_super_option_value<T>) {
            h.on_super_option_value(s);
        }
    }

    template<typename T>
    void
    emit_end_entry(T& h, iterator const it)
    {
        if constexpr (has_end_entry<T>) {
            h.end_entry(it);
        }
    }

    template<typename T>
    void
    emit_error(T& h, std::error_code const e, iterator const it,
               std::source_location loc = std::source_location::current())
    {
        if constexpr (has_on_error<T>) {
            h.on_error(e, it, std::move(loc));
        }
    }

    template<typename T>
    void
    emit_new_line(T& h, auto const it)
    {
        if constexpr (has_on_new_line<T>) {
            h.on_new_line(it);
        }
    }

    inline bool
    is_space(char const c) noexcept
    {
        return c == ' ' || c == '\t';
    }

    inline bool
    is_cr(char const c) noexcept
    {
        return c == '\n';
    }

    inline bool
    is_char(char const c) noexcept
    {
        return !is_space(c) && !is_cr(c);
    }

    inline void
    skip_space(iterator& it, iterator const end) noexcept
    {
        while (it != end && is_space(*it)) {
            ++it;
        }
    }

    inline void
    skip_to_next_line(iterator& it, iterator const end) noexcept
    {
        while (it != end) {
            if (is_cr(*it)) {
                ++it;
                break;
            }
            else {
                ++it;
            }
        }
    }

    enum result {
        fail = 0,
        success = 1,
        end_optional = 2,
    };

    inline result
    element(iterator& it, iterator const end, auto&& h, auto&& h1)
    {
        skip_space(it, end);

        if (it == end) {
            emit_error(h, errc::premature_end_of_text, it);
            return fail;
        }
        if (is_cr(*it)) {
            emit_error(h, errc::premature_end_of_line, it);
            return fail;
        }

        assert(is_char(*it));

        auto const start = it;

        while (true) {
            if (it == end || !is_char(*it)) {
                h1(std::string_view { start, it });
                break;
            }
            else {
                ++it;
            }
        }

        return success;
    }

    template<typename H>
    result
    super_option(iterator& it, iterator const end, H&& h)
    {
        skip_space(it, end);

        if (it == end) {
            emit_error(h, errc::premature_end_of_text, it);
            return fail;
        }
        else if (is_cr(*it)) {
            emit_error(h, errc::premature_end_of_line, it);
            return fail;
        }

        assert(is_char(*it));

        auto const first = it;
        auto value_first = end;

        while (true) {
            if (it == end || !is_char(*it) || *it == ',') {
                if (value_first == end) {
                    emit_super_option(h, { first, it });
                    emit_super_option_value(h, { it, it });
                }
                else {
                    emit_super_option_value(h, { value_first, it });
                }

                break;
            }
            else if constexpr (has_on_super_option_value<H>) {
                if (*it == '=') {
                    emit_super_option(h, { first, it });
                    ++it;

                    value_first = it;
                }
                else {
                    ++it;
                }
            }
            else {
                ++it;
            }
        }

        return success;
    }

    inline result
    super_options(iterator& it, iterator const end, auto&& h)
    {
        skip_space(it, end);

        auto first = it;

        auto r = super_option(it, end, h);
        if (r != success) return r;

        auto last = it;
        skip_space(it, end);

        while (it != end && *it == ',') {
            ++it;

            auto r = super_option(it, end, h);
            if (r != success) return r;

            last = it;
            skip_space(it, end);
        }

        emit_super_options(h, std::string_view { first, last });

        return success;
    }

    inline result
    mount_source(iterator& it, iterator const end, auto&& h)
    {
        return element(it, end, h, [&](auto s) {
            emit_mount_source(h, s);
        });
    }

    template<typename H>
    result
    filesystem_type(iterator& it, iterator const end, H&& h)
    {
        skip_space(it, end);

        if (it == end) {
            emit_error(h, errc::premature_end_of_text, it);
            return fail;
        }
        else if (is_cr(*it)) {
            emit_error(h, errc::premature_end_of_line, it);
            return fail;
        }

        assert(is_char(*it));

        auto const first = it;
        auto sub_first = end;

        while (true) {
            if (it == end || !is_char(*it)) {
                if (sub_first == end) {
                    emit_filesystem_type(h, { first, it });
                    emit_filesystem_subtype(h, { it, it });
                }
                else {
                    emit_filesystem_subtype(h, { sub_first, it });
                }
                break;
            }
            else if constexpr (has_on_filesystem_subtype<H>) {
                if (*it == '.') {
                    emit_filesystem_type(h, { first, it });
                    ++it;

                    sub_first = it;
                }
                else {
                    ++it;
                }
            }
            else {
                ++it;
            }
        }

        return success;
    }

    template<typename H>
    result
    optional_field(iterator& it, iterator const end, H&& h)
    {
        skip_space(it, end);

        if (it == end) {
            emit_error(h, errc::premature_end_of_text, it);
            return fail;
        }
        else if (is_cr(*it)) {
            emit_error(h, errc::premature_end_of_line, it);
            return fail;
        }

        assert(is_char(*it));

        auto const start = it;
        auto value_first = end;

        if (*it == '-') {
            ++it;
            if (it == end || is_space(*it) || is_cr(*it)) {
                return end_optional;
            }
        }

        while (true) {
            if (it == end || !is_char(*it)) {
                if (value_first == end) {
                    emit_optional_field(h, { start, it });
                    emit_optional_value(h, { it, it });
                }
                else {
                    emit_optional_value(h, { value_first, it });
                }

                break;
            }
            else if constexpr (has_on_optional_value<H>) {
                if (*it == ':') {
                    emit_optional_field(h, { start ,it });

                    ++it;
                    value_first = it;
                }
                else {
                    ++it;
                }
            }
            else {
                ++it;
            }
        }

        return success;
    }

    inline result
    optional_fields(iterator& it, iterator const end, auto&& h)
    {
        skip_space(it, end);

        if (it == end) {
            emit_error(h, errc::premature_end_of_text, it);
            return fail;
        }
        else if (is_cr(*it)) {
            emit_error(h, errc::premature_end_of_line, it);
            return fail;
        }

        auto first = it;
        auto last = it;

        while (true) {
            auto const r = optional_field(it, end, h);
            if (r == fail) return fail;
            if (r == end_optional) break;

            last = it;
        }

        emit_optional_fields(h, { first, last });

        return success;
    }

    template<typename H>
    result
    mount_option(iterator& it, iterator const end, H&& h)
    {
        skip_space(it, end);

        if (it == end) {
            emit_error(h, errc::premature_end_of_text, it);
            return fail;
        }
        else if (is_cr(*it)) {
            emit_error(h, errc::premature_end_of_line, it);
            return fail;
        }

        assert(is_char(*it));

        auto const first = it;
        auto value_first = end;

        while (true) {
            if (it == end || !is_char(*it) || *it == ',') {
                if (value_first == end) {
                    emit_mount_option(h, { first, it });
                    emit_mount_option_value(h, { it, it });
                }
                else {
                    emit_mount_option_value(h, { value_first, it });
                }

                break;
            }
            else if constexpr (has_on_mount_option_value<H>) {
                if (*it == '=') {
                    emit_mount_option(h, { first, it });
                    ++it;

                    value_first = it;
                }
                else {
                    ++it;
                }
            }
            else {
                ++it;
            }
        }

        return success;
    }

    inline result
    mount_options(iterator& it, iterator const end, auto&& h)
    {
        skip_space(it, end);

        auto first = it;

        auto r = mount_option(it, end, h);
        if (r != success) return r;

        auto last = it;
        skip_space(it, end);

        while (it != end && *it == ',') {
            ++it;

            auto r = mount_option(it, end, h);
            if (r != success) return r;

            last = it;
            skip_space(it, end);
        }

        emit_mount_options(h, std::string_view { first, last });

        return success;
    }

    inline result
    mount_point(iterator& it, iterator const end, auto&& h)
    {
        return element(it, end, h, [&](auto s) {
            emit_mount_point(h, s);
        });
    }

    inline result
    root(iterator& it, iterator const end, auto&& h)
    {
        return element(it, end, h, [&](auto s) {
            emit_root(h, s);
        });
    }

    template<typename H>
    result
    device_id(iterator& it, iterator const end, H&& h)
    {
        skip_space(it, end);

        if (it == end) {
            emit_error(h, errc::premature_end_of_text, it);
            return fail;
        }
        else if (is_cr(*it)) {
            emit_error(h, errc::premature_end_of_line, it);
            return fail;
        }

        assert(is_char(*it));

        auto const start = it;
        auto minor_start = end;

        while (true) {
            if (it == end || !is_char(*it)) {
                if (minor_start != end) {
                    emit_device_minor(h, { minor_start, it });
                }
                else {
                    emit_device_id(h, { start, it });
                    emit_device_minor(h, { it, it });
                }

                break;
            }
            else if constexpr (has_on_device_minor<H>) {
                if (*it == ':') {
                    emit_device_id(h, { start, it });

                    ++it;
                    minor_start = it;
                }
                else {
                    ++it;
                }
            }
            else {
                ++it;
            }
        }

        return success;
    }

    inline result
    parent_id(iterator& it, iterator const end, auto&& h)
    {
        return element(it, end, h, [&](auto s) {
            emit_parent_id(h, s);
        });
    }

    inline result
    mount_id(iterator& it, iterator const end, auto&& h)
    {
        return element(it, end, h, [&](auto s) {
            emit_mount_id(h, s);
        });
    }

    inline result
    entry(iterator& it, iterator const end, auto&& h)
    {
        emit_begin_entry(h, it);

        auto r = mount_id(it, end, h);
        if (r != success) {
            return r;
        }

        r = parent_id(it, end, h);
        if (r != success) {
            return r;
        }

        r = device_id(it, end, h);
        if (r != success) {
            return r;
        }

        r = root(it, end, h);
        if (r != success) {
            return r;
        }

        r = mount_point(it, end, h);
        if (r != success) {
            return r;
        }

        r = mount_options(it, end, h);
        if (r != success) {
            return r;
        }

        r = optional_fields(it, end, h);
        if (r != success) {
            return r;
        }

        r = filesystem_type(it, end, h);
        if (r != success) {
            return r;
        }

        r = mount_source(it, end, h);
        if (r != success) {
            return r;
        }

        r = super_options(it, end, h);
        if (r != success) {
            return r;
        }

        emit_end_entry(h, it);

        return r;
    }

    template<typename H>
    bool
    mountinfo(iterator& it, iterator const end, H&& h)
    {
        try {
            while (it != end) {
                try {
                    auto const r = entry(it, end, h);
                    if (r == fail) {
                        return false;
                    }

                    skip_space(it, end);

                    if (it == end) break;

                    if (is_cr(*it)) {
                        ++it;
                        emit_new_line(h, it);
                    }
                    else {
                        emit_error(h, errc::invalid_character, it);
                        return false;
                    }
                }
                catch (skip_line const&) {
                    skip_to_next_line(it, end);
                }
            }
        }
        catch (exit const&) {
           // nop
        }

        return true;
    }

} // namespace parser

template<typename EventHandler>
bool
parse(std::string_view const s, EventHandler&& h)
{
    auto it = s.begin();
    return parser::mountinfo(it, s.end(), std::forward<EventHandler>(h));
}

inline std::error_category const&
error_category()
{
    static struct impl : std::error_category
    {
        char const* name() const noexcept
        {
            return "stream9::linux::proc::mountinfo";
        }

        std::string message(int const e) const
        {
            switch (static_cast<errc>(e)) {
                using enum errc;
                case invalid_character:
                    return "invalid character";
                case premature_end_of_line:
                    return "premature end of line";
                case premature_end_of_text:
                    return "premature end of text";
            }

            return "unknown error";
        }
    } instance;

    return instance;
}

inline std::error_code
make_error_code(errc const e) noexcept
{
    return { static_cast<int>(e), error_category() };
}

} // namespace stream9::linux::proc::mountinfo

namespace std {

    template<>
    struct is_error_code_enum<stream9::linux::proc::mountinfo::errc>
        : true_type {};

} // namespace std

#endif // STREAM9_LINUX_PROC_MOUNTINFO_IPP
